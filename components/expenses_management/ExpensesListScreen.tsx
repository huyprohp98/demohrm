import React from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../../constaints/Colors';
import Icons from '../../constaints/Icons';
import Images from '../../constaints/Images';
import AppbarLogin from '../shared/AppbarLogin';

const DATA_MENU = [
  {
    id: '1',
    image: Images.imgPersonOneEmployee,
    titleName: 'Shaidul Islam',
    titleContent: 'Designer',
  },
  {
    id: '2',
    image: Images.imgPersonTwoEmployee,
    titleName: 'Mehedii Mohammad',
    titleContent: 'Manager',
  },
  {
    id: '3',
    image: Images.imgPersonThreeEmployee,
    titleName: 'Ibne Riead',
    titleContent: 'Developer',
  },
  {
    id: '4',
    image: Images.imgPersonFourEmployee,
    titleName: 'Emily',
    titleContent: 'Officer',
  },
];

interface Props {
  navigation?: any;
}
interface itemProps {
  item?: any;
}
const ExpensesListScreen: React.FC<Props> = props => {
  const renderItemMenu: React.FC<itemProps> = ({item}) => (
    <View style={styles.containerList}>
      <TouchableOpacity
        style={styles.containerBtn}
        onPress={() => {
          props.navigation.navigate('ExpensesDetail', {data: item});
        }}>
        <View
          style={[
            styles.containerIconBtn,
            {backgroundColor: item.colorContainerIcon},
          ]}>
          <Image source={item.image} />
        </View>
        <View style={styles.containerContentText}>
          <Text style={styles.contentText}>{item.titleName}</Text>
          <Text
            style={[
              styles.contentText,
              {color: '#9090AD', fontSize: 13, fontWeight: '400'},
            ]}>
            {item.titleContent}
          </Text>
        </View>

        <Icon
          name={'chevron-forward-outline'}
          size={24}
          style={{marginRight: 25}}
        />
      </TouchableOpacity>
    </View>
  );
  return (
    <View style={styles.container}>
      <AppbarLogin
        navigation={props.navigation}
        content={'Expenses List'}
        iconRight={true}
      />
      <View style={styles.containerContent}>
        <View style={styles.containerContentMenu}>
          <FlatList
            data={DATA_MENU}
            renderItem={renderItemMenu}
            keyExtractor={item => item.id}
          />
        </View>
        <TouchableOpacity
          style={styles.btnAdd}
          onPress={() => {
            // props.navigation.navigate('ExpensesDetail', {
            //   data: 'ExpensesDetail',
            // });
          }}>
          <Image source={Icons.icAddEmployee} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: '#FAFAFA',
  },
  containerContentMenu: {
    flex: 9,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    marginTop: 20,
    paddingHorizontal: 24,
    paddingTop: 24,
  },
  containerList: {
    marginBottom: 16,
  },
  containerBtn: {
    height: 56,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'rgba(34, 33, 91, 0.16)',
    backgroundColor: Colors.while,
    flexDirection: 'row',
    alignItems: 'center',
  },
  colorBorderRight: {
    position: 'absolute',
    width: 4,
    height: 56,
    borderBottomLeftRadius: 6,
    borderTopLeftRadius: 6,
  },
  containerIconBtn: {
    marginHorizontal: 16,
    marginTop: 16,
    marginBottom: 12,
    borderRadius: 6,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerContentText: {flex: 8},
  contentText: {
    marginHorizontal: 16,
    fontSize: 16,
    fontWeight: '400',
    color: '#22215B',
  },
  btnAdd: {position: 'absolute', bottom: 60, right: 34},
});
export default ExpensesListScreen;
