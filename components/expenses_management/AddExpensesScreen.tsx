/* eslint-disable react/self-closing-comp */
import React, {createRef, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../../constaints/Colors';
import AppbarLogin from '../shared/AppbarLogin';
import BtnSelect from '../shared/BtnSelect';
import BtnSelectbottomSheet from '../shared/BtnSelectbottomSheet';
import BtnSelectDate from '../shared/BtnSelectDate';
import Buttons from '../shared/Buttons';
import TextInputLogin from '../shared/TextInputLogin';

interface Props {
  navigation: any;
}
const AddExpensesScreen: React.FC<Props> = ({navigation}) => {
  const refRBSheet = createRef<RBSheet>();
  const [setEmployee, getEmployee] = useState('Shaidul Islam');
  const [setValueTextInput, getValueTextInput] = useState('');
  const [setTwoValueTextInput, getTwoValueTextInput] = useState('');

  const handleGetValueEmployee = (value: string) => {
    getEmployee(value);
    refRBSheet.current?.close();
  };

  const dataPurpose = [
    {value: 'Marketing', isSelected: true},
    {value: 'Design', isSelected: false},
    {value: 'Marketing', isSelected: false},
  ];
  const dataEmployee = [
    {value: 'Shaidul Islam', isSelected: true},
    {value: 'Mehedii Mohammad', isSelected: false},
  ];
  return (
    <View style={styles.container}>
      <AppbarLogin
        navigation={navigation}
        content={'Add Expenses Management'}
      />
      <View style={styles.containerContent}>
        <View>
          <View style={styles.labelContainer}>
            <Text>Select Employee</Text>
          </View>
          <TouchableOpacity
            style={styles.pickerContainer}
            onPress={() => refRBSheet.current?.open()}>
            <Text style={styles.contentPickker}> {setEmployee}</Text>
            <Icon name={'chevron-down-outline'} size={24} />
          </TouchableOpacity>
        </View>
        <RBSheet
          ref={refRBSheet}
          closeOnDragDown={false}
          height={200}
          customStyles={{
            wrapper: {
              backgroundColor: 'transparent',
            },
          }}>
          <BtnSelectbottomSheet
            dataEmployee={dataEmployee}
            handleGetValueEmployee={
              handleGetValueEmployee
            }></BtnSelectbottomSheet>
        </RBSheet>

        <BtnSelectDate
          title={'Expenses Date'}
          placeholder={'14/8/2021'}
          borderColor={'rgba(34, 33, 91, 0.16)'}></BtnSelectDate>
        <BtnSelect
          title={'Expence Purpose'}
          placeholder={'Marketing'}
          borderColor={'rgba(34, 33, 91, 0.16)'}
          data={dataPurpose}
          icon={'chevron-down-outline'}
        />
        <TextInputLogin
          titleText={'Expence Amount'}
          placeholder={'$10,000'}
          borderColor={'rgba(34, 33, 91, 0.16)'}
          marginBottom={16}
          backgroundColor={Colors.whileBackground}
          callGetValueTextInput={getValueTextInput}
        />
        <TextInputLogin
          titleText={'Cheque No.?'}
          placeholder={'Enter any code'}
          borderColor={'rgba(34, 33, 91, 0.16)'}
          marginBottom={16}
          backgroundColor={Colors.whileBackground}
          callGetValueTextInput={getTwoValueTextInput}
        />
        <View style={styles.BtnBottom}>
          <Buttons
            backgroundColor={Colors.blueStraight}
            height={54}
            borderRadius={10}
            content={'Save'}
            fontSizeContent={20}
            fontWeightContent={'600'}
            color={Colors.while}
            onPress={() => {
              navigation.navigate('ExpensesListScreen');
            }}
          />
        </View>
        <Text>{setValueTextInput}</Text>
        <Text>{setTwoValueTextInput}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.whileBackground,
    paddingTop: 24,
    paddingHorizontal: 24,
  },
  BtnBottom: {
    position: 'absolute',
    width: '100%',
    bottom: 20,
    marginHorizontal: 24,
  },
  labelContainer: {
    backgroundColor: 'white',
    alignSelf: 'flex-start',
    paddingHorizontal: 10,
    marginLeft: 10,
    zIndex: 1,
    elevation: 1,
    shadowColor: 'white',
    position: 'absolute',
    top: -12,
    fontSize: 14,
    color: Colors.blackText,
  },
  pickerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 20,
    zIndex: 0,
    marginBottom: 16,
    height: 54,
    borderColor: 'rgba(34, 33, 91, 0.16)',
  },
  contentPickker: {
    fontSize: 14,
    color: '#9090AD',
  },
});
export default AddExpensesScreen;
