/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/self-closing-comp */
import React from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Colors from '../../constaints/Colors';
import Icons from '../../constaints/Icons';
import Images from '../../constaints/Images';

interface Props {
  navigation?: any;
}

const ExpensesPDF: React.FC<Props> = props => {
  const dataText = [
    {
      id: '1',
      sl: '1',
      purpose: 'Marketing',
      amount: '$10.00',
    },
    {
      id: '2',
      sl: '2',
      purpose: '1254',
      amount: '$20.00',
    },
  ];
  return (
    <View style={styles.container}>
      <View style={styles.containerAppbar}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Image source={Icons.icArrowLeft} style={styles.btnBack}></Image>
        </TouchableOpacity>
        <TouchableOpacity style={styles.containerBtnPrint}>
          <Text style={styles.text}>Print</Text>
        </TouchableOpacity>
      </View>
      <View
        style={[
          // styles.containerAppbar,
          {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'flex-end',
            marginBottom: 20,
          },
        ]}>
        <Image source={Images.imgLogoTitleHrm} style={styles.imageLogo}></Image>
        <View style={{alignItems: 'flex-end'}}>
          <Text
            style={[
              styles.text,
              {fontWeight: '500', fontSize: 16, color: Colors.blackText},
            ]}>
            Maan Theme
          </Text>
          <Text
            style={[
              styles.text,
              {fontWeight: '500', fontSize: 14, color: '#9090AD'},
            ]}>
            Admin
          </Text>
          <Text style={styles.textContent}>14-08-2021</Text>
        </View>
      </View>
      <View style={styles.straight}></View>
      <View
        style={{flexDirection: 'row', marginTop: 16, justifyContent: 'center'}}>
        <Text style={[styles.textContent, {flex: 4}]}>Cheque No</Text>
        <Text style={[styles.textContent, {flex: 1}]}>:</Text>
        <Text style={[styles.textContent, {flex: 5}]}>1452</Text>
      </View>
      <View
        style={{flexDirection: 'row', marginTop: 8, justifyContent: 'center'}}>
        <Text style={[styles.textContent, {flex: 4}]}>Pay By Email</Text>
        <Text style={[styles.textContent, {flex: 1}]}>:</Text>
        <Text style={[styles.textContent, {flex: 5}]}>maantheme@gmail.com</Text>
      </View>
      <View
        style={{flexDirection: 'row', marginTop: 8, justifyContent: 'center'}}>
        <Text style={[styles.textContent, {flex: 4}]}>Pay By Number</Text>
        <Text style={[styles.textContent, {flex: 1}]}>:</Text>
        <Text style={[styles.textContent, {flex: 5, marginBottom: 16}]}>
          14-08-2021
        </Text>
      </View>
      <View style={[styles.straight, {marginBottom: 16}]}></View>
      <View style={styles.information}>
        <Text style={styles.textInformation}>S/L</Text>
        <Text style={styles.textInformation}>Purpose</Text>
        <Text style={styles.textInformation}>Amount</Text>
      </View>
      <View style={[styles.straight, {marginTop: 8, marginBottom: 16}]}></View>
      <View>
        <FlatList
          data={dataText}
          renderItem={value => (
            <View>
              <View style={styles.information}>
                <Text>{value.item.sl}</Text>
                <Text>{value.item.purpose}</Text>
                <Text>{value.item.amount}</Text>
              </View>
              <View style={[styles.straight, {marginVertical: 16}]}></View>
            </View>
          )}
          keyExtractor={item => item.id}
        />
      </View>
      <View style={styles.information}>
        <Text style={[styles.textInformation, {fontWeight: '400'}]}>Total</Text>
        <Text style={[styles.textInformation, {fontWeight: '400'}]}>
          $30.00
        </Text>
      </View>
      <View style={[styles.information, {marginTop: 65, marginBottom: 4}]}>
        <View style={[styles.straight, {flex: 4}]}></View>
        <View style={{flex: 1}}></View>
        <View style={[styles.straight, {flex: 4}]}></View>
      </View>
      <View style={[styles.information]}>
        <Text
          style={[
            styles.textInformation,
            {fontWeight: '400', color: '#9090AD'},
          ]}>
          Authorize Signature
        </Text>
        <Text
          style={[
            styles.textInformation,
            {fontWeight: '400', color: '#9090AD'},
          ]}>
          Administration Signature
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 24,
  },
  containerAppbar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 16,
    marginBottom: 40,
  },
  btnBack: {
    tintColor: 'black',
    width: 24,
    height: 24,
  },
  containerBtnPrint: {
    width: 91,
    height: 32,
    backgroundColor: '#567DF4',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
  },
  text: {
    fontSize: 16,
    fontWeight: '700',
    color: Colors.while,
  },
  imageLogo: {
    width: 61,
    height: 35,
    resizeMode: 'cover',
  },
  straight: {
    width: '100%',
    height: 1,
    backgroundColor: '#E5E5E5',
  },
  textContent: {color: '#9090AD', fontWeight: '400'},
  information: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textInformation: {
    fontSize: 14,
    fontWeight: '600',
    color: '#22215B',
  },
});
export default ExpensesPDF;
