import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
} from 'react-native';
import React from 'react';
import AppbarLogin from '../shared/AppbarLogin';
import Colors from '../../constaints/Colors';
import Icon from 'react-native-vector-icons/Ionicons';
import Icons from '../../constaints/Icons';

const DATA_MENU = [
  {
    id: '1',
    colorBorderRight: '#FD74B0',
    colorContainerIcon: 'rgba(253, 118, 177, 0.08)',
    icon: Icons.icExpensesHome,
    titleName: 'Expenses',
  },
];

interface Props {
  navigation: any;
}
interface Item {
  item?: any;
  index?: number;
}
const ScreenExpenses: React.FC<Props> = props => {
  const renderItemMenu: React.FC<Item> = ({item, index}) => (
    <View style={styles.containerList}>
      <TouchableOpacity
        style={styles.containerBtn}
        onPress={() =>
          index === 0
            ? props.navigation.navigate('NoDataEmPloyeeSceen', {
                title: item.titleName,
              })
            : null
        }>
        <View
          style={[
            styles.colorBorderRight,
            {backgroundColor: item.colorBorderRight},
          ]}
        />
        <View
          style={[
            styles.containerIconBtn,
            {backgroundColor: item.colorContainerIcon},
          ]}>
          <Image source={item.icon} />
        </View>
        <Text style={styles.contentBtn}>{item.titleName}</Text>

        <Icon
          name={'chevron-forward-outline'}
          size={24}
          style={{marginRight: 60}}
        />
      </TouchableOpacity>
    </View>
  );
  return (
    <View style={styles.container}>
      <AppbarLogin
        navigation={props.navigation}
        content={'Expenses Management'}
      />
      <View style={styles.containerContent}>
        <View style={styles.containerContentMenu}>
          <FlatList
            data={DATA_MENU}
            renderItem={renderItemMenu}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.whileBackground,
    paddingTop: 24,
  },
  containerContentMenu: {
    flex: 1,
    marginTop: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 24,
    paddingTop: 24,
  },
  containerList: {
    marginBottom: 16,
  },

  containerBtn: {
    width: Dimensions.get('window').width,
    height: 56,
    borderRadius: 6,
    backgroundColor: Colors.while,
    flexDirection: 'row',
    alignItems: 'center',
  },
  colorBorderRight: {
    position: 'absolute',
    width: 4,
    height: 56,
    borderBottomLeftRadius: 6,
    borderTopLeftRadius: 6,
  },
  containerIconBtn: {
    marginHorizontal: 16,
    marginTop: 16,
    marginBottom: 12,
    borderRadius: 6,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentBtn: {
    flex: 8,
    marginHorizontal: 16,
    fontSize: 16,
    fontWeight: '400',
    color: '#22215B',
  },
});
export default ScreenExpenses;
