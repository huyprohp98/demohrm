import React from 'react';
import {FlatList, ScrollView, StyleSheet, Text, View} from 'react-native';
import Colors from '../../constaints/Colors';
import Icons from '../../constaints/Icons';
import AppbarDetailEmployee from '../shared/AppbarDetailEmployee';
import Buttons from '../shared/Buttons';
import TextInputLogin from '../shared/TextInputLogin';

interface Props {
  route?: any;
  navigation?: any;
}

const dataText = [
  {
    id: '1',
    date: '14/08/221',
    amount: '1452',
    price: '$10.00',
  },
  {
    id: '2',
    date: '15/08/221',
    amount: '1452',
    price: '$10.00',
  },
  {
    id: '3',
    date: '16/08/221',
    amount: '1452',
    price: '$10.00',
  },
];
const ExpensesDetail: React.FC<Props> = props => {
  console.log(props.route.params.data);

  const data = props.route.params.data;
  return (
    <View style={styles.container}>
      <AppbarDetailEmployee
        content={data.titleName}
        noteAppbar={data.titleContent}
        imgLeft={data.image}
        imgRight={Icons.icDelete}
        navigation={props.navigation}
      />
      <View style={styles.containerContent}>
        <View style={styles.containerInputTop}>
          <TextInputLogin
            titleText={'Created By'}
            placeholder={'Maan theme'}
            borderColor={'rgba(34, 33, 91, 0.16)'}
            marginBottom={16}
            flex={5}
            marginRight={15}
          />
          <Buttons
            marginRight={10}
            backgroundColor={Colors.blueStraight}
            height={54}
            borderRadius={10}
            content={'Pay Slip'}
            fontSizeContent={20}
            fontWeightContent={'600'}
            color={Colors.while}
            flex={5}
          />
        </View>
        <View style={styles.containerContentInformation}>
          <View style={styles.information}>
            <Text style={styles.textInformation}>Date</Text>
            <Text style={[styles.textInformation, {marginLeft: 50}]}>
              Cheque No
            </Text>
            <Text style={styles.textInformation}>Amount</Text>
          </View>
          <View style={styles.straight} />
          <FlatList
            data={dataText}
            renderItem={value => (
              <View style={styles.information}>
                <Text>{value.item.date}</Text>
                <Text>{value.item.amount}</Text>
                <Text>{value.item.price}</Text>
              </View>
            )}
            keyExtractor={item => item.id}
          />

          <View style={styles.straight} />
          <View style={styles.information}>
            <Text style={styles.textInformation}>Total</Text>
            <Text style={styles.textInformation}>$175.00</Text>
          </View>
        </View>

        <View style={styles.btnBottom}>
          <Buttons
            marginRight={10}
            backgroundColor={Colors.blueStraight}
            height={54}
            borderRadius={10}
            content={'Delete'}
            fontSizeContent={20}
            fontWeightContent={'600'}
            color={Colors.while}
            flex={5}
          />
          <Buttons
            backgroundColor={'rgba(86, 125, 244, 0.1)'}
            height={54}
            borderRadius={10}
            content={'Edit'}
            fontSizeContent={20}
            fontWeightContent={'600'}
            color={Colors.while}
            flex={5}
            onPress={() => props.navigation.navigate('ExpensesPDF')}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: '#FAFAFA',
    paddingTop: 40,
  },
  containerContentMenu: {
    flex: 9,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    marginTop: 20,
    paddingHorizontal: 24,
    paddingTop: 24,
    backgroundColor: '#FFFFFF',
    // position: 'absolute',
  },
  containerInputTop: {flexDirection: 'row', marginHorizontal: 24},
  containerContentInformation: {
    marginHorizontal: 24,
    marginBottom: 24,
    height: 202,
    backgroundColor: Colors.while,
    borderRadius: 20,
    paddingHorizontal: 16,
    paddingVertical: 24,
  },
  information: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // alignItems: 'stretch',
  },
  straight: {
    height: 1,
    backgroundColor: '#22215B',
    marginTop: 10,
    marginBottom: 20,
  },
  textInformation: {
    fontSize: 14,
    fontWeight: '600',
    color: '#22215B',
  },
  btnBottom: {
    flexDirection: 'row',
    marginHorizontal: 24,
    position: 'absolute',
    bottom: 10,
  },
});
export default ExpensesDetail;
