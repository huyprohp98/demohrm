/* eslint-disable react-native/no-inline-styles */
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import AppbarLogin from '../shared/AppbarLogin';
import TextInputLogin from '../shared/TextInputLogin';
import TextInputFlagLogin from '../shared/TextInputFlagLogin';
import Buttons from '../shared/Buttons';
import Colors from '../../constaints/Colors';
import BtnCheckAccount from '../shared/BtnCheckAccount';

interface Props {
  navigation?: any;
}

const SignIn: React.FC<Props> = ({navigation}) => {
  const [saveAcount, setSaveAcount] = useState(false);
  return (
    <View style={styles.container}>
      <AppbarLogin
        navigation={navigation}
        content={'Sign In'}
        noteAppbar={'Sign In now to begin an amazing journey'}
      />
      <View style={styles.containerContent}>
        <TextInputLogin titleText={'Employee ID'} placeholder={'125462'} />
        <TextInputFlagLogin title={'Mobile Number'} navigation={navigation} />
        <TextInputLogin
          titleText={'Password'}
          placeholder={'**********'}
          checkPassword={true}
        />
        <Buttons
          backgroundColor={Colors.blueStraight}
          marginTop={30}
          marginBottom={16}
          height={54}
          borderRadius={10}
          content={'Sign In'}
          fontSizeContent={20}
          fontWeightContent={'600'}
          color={Colors.while}
          onPress={() => {
            navigation.navigate('Home', {name: 'Home'});
          }}
        />
        <View style={styles.containerSaveForgot}>
          <TouchableOpacity
            onPress={() => {
              setSaveAcount(!saveAcount);
            }}
            style={[
              styles.containerSaveAcount,
              {
                backgroundColor:
                  saveAcount === true ? Colors.blueStraight : null,
                borderColor: saveAcount === true ? 'rgba(34, 33, 91, 1)' : null,
              },
            ]}>
            <View
              style={[
                styles.saveAcount,
                {
                  backgroundColor:
                    saveAcount === true ? Colors.blueStraight : '#9090AD',
                },
              ]}
            />
          </TouchableOpacity>
          <Text style={styles.textSave}> Save Me</Text>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ScreenForgotPassword', {
                name: 'ScreenForgotPassword',
              });
            }}>
            <Text>Forgot Password ?</Text>
          </TouchableOpacity>
        </View>
        <BtnCheckAccount
          title={'Dont’t have an account?'}
          content={' SIGN UP'}
          onPress={() => {
            navigation.navigate('SignUpEmployee', {name: 'SignUpEmployee'});
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.while,
    paddingHorizontal: 24,
    paddingTop: 40,
  },
  containerSaveForgot: {
    marginTop: 16,
    marginBottom: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerSaveAcount: {
    width: 32,
    height: 16,
    borderRadius: 8,
    borderWidth: 1,
    marginRight: 6,
    justifyContent: 'center',
  },
  saveAcount: {
    width: 8,
    height: 8,
    borderRadius: 100,
    marginLeft: 4,
  },
  textSave: {
    marginRight: 145,
  },
});
export default SignIn;
