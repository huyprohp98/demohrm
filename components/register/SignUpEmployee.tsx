import React from 'react';
import {StyleSheet, View} from 'react-native';
import Colors from '../../constaints/Colors';
import AppbarLogin from '../shared/AppbarLogin';
import BtnCheckAccount from '../shared/BtnCheckAccount';
import Buttons from '../shared/Buttons';
import TextInputFlagLogin from '../shared/TextInputFlagLogin';
import TextInputLogin from '../shared/TextInputLogin';

interface Props {
  navigation?: any;
}

const SignUpEmployee: React.FC<Props> = ({navigation}) => {
  return (
    <View style={styles.container}>
      <AppbarLogin
        navigation={navigation}
        content={'Sign Up Employee'}
        noteAppbar={'Sign Up now to begin an amazing journey'}
      />
      <View style={styles.containerContent}>
        <TextInputLogin
          titleText={'Employee Name'}
          placeholder={'Ibne Riead'}
        />
        <TextInputLogin
          titleText={'Employee ID'}
          placeholder={'125462'}
          numeric={'numeric'}
        />
        <TextInputFlagLogin title={'Mobile Number'} navigation={navigation} />
        <TextInputLogin
          titleText={'Password'}
          placeholder={'**********'}
          checkPassword={true}
          numeric={'numeric'}
        />
        <Buttons
          backgroundColor={Colors.blueStraight}
          marginTop={30}
          marginBottom={16}
          height={54}
          borderRadius={10}
          content={'Sign Up'}
          fontSizeContent={20}
          fontWeightContent={'600'}
          color={Colors.while}
          onPress={() => {
            navigation.navigate('ScreenOtp', {name: 'ScreenOtp'});
          }}
        />
        <BtnCheckAccount
          title={'Have an account?'}
          content={' SIGN IN'}
          onPress={() => {
            navigation.navigate('SignIn', {name: 'SignIn'});
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.while,
    paddingHorizontal: 24,
    paddingTop: 40,
  },
});
export default SignUpEmployee;
