import React, {useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Colors from '../../constaints/Colors';
import Icons from '../../constaints/Icons';
import AppbarLogin from '../shared/AppbarLogin';
import BtnCheckAccount from '../shared/BtnCheckAccount';
import BtnSelect from '../shared/BtnSelect';
import Buttons from '../shared/Buttons';
import TextInputFlagLogin from '../shared/TextInputFlagLogin';
import TextInputLogin from '../shared/TextInputLogin';

interface Props {
  navigation?: any;
}

const SignUpManage: React.FC<Props> = ({navigation}) => {
  const dataEmployeeStrenght = [
    {value: '0-10', isSelected: true},
    {value: '11-20', isSelected: false},
    {value: '21-60', isSelected: false},
    {value: '61-100', isSelected: false},
    {value: '101-200', isSelected: false},
    {value: '201-400', isSelected: false},
  ];
  return (
    <View style={styles.container}>
      <AppbarLogin
        navigation={navigation}
        content={'Sign Up Owner / Admin / HR'}
        noteAppbar={'Sign Up now to begin an amazing journey'}
      />
      <View style={styles.containerContent}>
        <TextInputLogin titleText={'Company Name'} placeholder={'Dhaka IT'} />
        <TextInputLogin
          titleText={'Owner Full Name'}
          placeholder={'Maan Theme'}
        />
        <TextInputLogin
          titleText={'Email'}
          placeholder={'maantheme@gmail.com'}
        />
        <TextInputFlagLogin
          title={'Mobile Number'}
          navigation={navigation}
          numeric={'numeric'}
        />
        <TextInputLogin titleText={'Password'} placeholder={'**********'} />
        <BtnSelect
          placeholder={'0-10'}
          title={'Employee Strength'}
          data={dataEmployeeStrenght}
        />
        <Buttons
          backgroundColor={Colors.blueStraight}
          marginTop={20}
          marginBottom={16}
          height={54}
          borderRadius={10}
          content={'Sign Up'}
          fontSizeContent={20}
          fontWeightContent={'600'}
          color={Colors.while}
          onPress={() => {
            navigation.navigate('ScreenOtp', {name: 'ScreenOtp'});
          }}
        />
        <BtnCheckAccount
          title={'Have an account?'}
          content={' SIGN IN'}
          onPress={() => {
            navigation.navigate('SignIn', {name: 'SignIn'});
          }}
        />
        <Text style={styles.selectTitle}>Or Sign in with</Text>
        <View style={styles.rowSelect}>
          <TouchableOpacity>
            <Image source={Icons.icFacebookSignup} style={styles.btnFacebook} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image source={Icons.icGoogleSignup} style={styles.btnGoogle} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image source={Icons.icTwitterSignup} style={styles.btnTwitter} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 30,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.while,
    paddingHorizontal: 24,
    paddingTop: 24,
  },
  selectTitle: {
    marginTop: 40,
    marginHorizontal: 130,
    fontSize: 14,
    fontWeight: '400',
    marginBottom: 20,
  },
  rowSelect: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnFacebook: {width: 50, height: 50, marginLeft: 20},
  btnGoogle: {width: 50, height: 50, marginLeft: 20},
  btnTwitter: {width: 50, height: 50, marginLeft: 20},
});
export default SignUpManage;
