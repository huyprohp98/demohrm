import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import React from 'react';
import AppbarLogin from '../shared/AppbarLogin';
import Colors from '../../constaints/Colors';
import Icons from '../../constaints/Icons';
const dataFlag = [
  {
    source: Icons.icFlagUnitedStates,
    title: 'United States',
  },
  {
    source: Icons.icFlagUnitedKingdom,
    title: 'United Kingdom',
  },
  {
    source: Icons.icFlagItaly,
    title: 'Italy',
  },
  {
    source: Icons.icFlagGermany,
    title: 'Germany',
  },
  {
    source: Icons.icFlagFrance,
    title: 'France',
  },
  {
    source: Icons.icFlagBangladesh,
    title: 'Bangladesh',
  },
  {
    source: Icons.icFlagIndia,
    title: 'India',
  },
  {
    source: Icons.icFlagCanada,
    title: 'Canada',
  },
  {
    source: Icons.icFlagAustralia,
    title: 'Australia',
  },
  {
    source: Icons.icFlagUruguay,
    title: 'Uruguay',
  },
  {
    source: Icons.icFaTurkey,
    title: 'Turkey',
  },
];
interface countryProps {
  route?: any;
  navigation?: any;
}
interface ItemProps {
  item?: any;
  index?: any;
}
const ScreenCountry: React.FC<countryProps> = ({route, navigation}) => {
  const renderItem: React.FC<ItemProps> = ({item, index}) => (
    <TouchableOpacity
      style={styles.item}
      onPress={() => {
        route.params.backData(item, index + 1);
        navigation.goBack();
      }}>
      <Image source={item.source} style={styles.imageItem} />
      <Text style={styles.textItem}>{item.title}</Text>
    </TouchableOpacity>
  );
  return (
    <View style={styles.container}>
      <AppbarLogin navigation={navigation} content={'Country Code'} />
      <View style={styles.containerContent}>
        <FlatList
          data={dataFlag}
          renderItem={renderItem}
          keyExtractor={item => item.source}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.while,
    paddingHorizontal: 24,
    paddingTop: 40,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  imageItem: {
    width: 32,
    height: 32,
    resizeMode: 'cover',
    marginRight: 20,
  },
  textItem: {
    fontSize: 14,
    fontWeight: '400',
  },
});

export default ScreenCountry;
