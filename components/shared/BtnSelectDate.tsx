/* eslint-disable @typescript-eslint/no-shadow */
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import React, {useState} from 'react';
import Colors from '../../constaints/Colors';
import Icons from '../../constaints/Icons';
import DatePicker from 'react-native-date-picker';

interface Props {
  placeholder?: any;
  title?: string;
  borderColor?: any;
}
const BtnSelectDate: React.FC<Props> = props => {
  const [chooseData, setChooseData] = useState(props.placeholder);
  const [date, setDate] = useState(new Date());
  const [open, setOpen] = useState(false);
  // const [isModalVisible, setisModalVisible] = useState(false);
  const changeModalVisibility = bool => {
    setOpen(true);
    // setisModalVisible(bool);
  };

  return (
    <View>
      <View style={styles.labelContainer}>
        <Text>{props.title}</Text>
      </View>
      <TouchableOpacity
        style={[
          styles.pickerContainer,
          {borderColor: props.borderColor ? props.borderColor : null},
        ]}
        onPress={() => changeModalVisibility(true)}>
        <Text style={styles.contentPickker}>{chooseData}</Text>
        <Image source={Icons.icDate} />
      </TouchableOpacity>
      <DatePicker
        modal
        open={open}
        date={date}
        mode="datetime"
        // title={'Lựa chọn ngày'}
        onConfirm={(date: any) => {
          setOpen(false);
          setDate(date);
          // let YDAY = `${date.getSeconds()}/${date.getMinutes()}/${date.getHours()}/${date.getDate()}/${
          //   date.getMonth() + 1
          // }/${date.getFullYear()}`;
          let YDAY = `${date.getDate()}/${
            date.getMonth() + 1
          }/${date.getFullYear()}`;
          console.log('date             ', YDAY);
          setChooseData(YDAY);
        }}
        onCancel={() => {
          setOpen(false);
        }}
      />
      {/* <Modal
        animationType="slide"
        transparent={true}
        visible={isModalVisible}
        onRequestClose={() => changeModalVisibility(false)}>
      </Modal> */}
    </View>
  );
};

const styles = StyleSheet.create({
  labelContainer: {
    backgroundColor: 'white',
    alignSelf: 'flex-start',
    paddingHorizontal: 10,
    marginLeft: 10,
    zIndex: 1,
    elevation: 1,
    shadowColor: 'white',
    position: 'absolute',
    top: -12,
    fontSize: 14,
    color: Colors.blackText,
  },
  pickerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 20,
    zIndex: 0,
    marginBottom: 16,
    height: 54,
  },
  contentPickker: {
    fontSize: 14,
    color: '#9090AD',
  },
});
export default BtnSelectDate;
