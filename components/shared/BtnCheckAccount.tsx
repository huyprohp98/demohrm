import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import Colors from '../../constaints/Colors';

interface Props {
  onPress?: any;
  title?: any;
  content?: any;
}

const BtnCheckAccount: React.FC<Props> = props => {
  return (
    <TouchableOpacity style={styles.btnSignIn} onPress={props.onPress}>
      <Text style={styles.content}>{props.title}</Text>
      <Text style={[styles.content, {color: Colors.blueStraight}]}>
        {props.content}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btnSignIn: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  content: {
    fontSize: 13,
  },
});
export default BtnCheckAccount;
