import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import React, {useState} from 'react';
import Icons from '../../constaints/Icons';
import Colors from '../../constaints/Colors';

interface Props {
  navigation?: any;
  title?: any;
  numeric?: any;
}

const TextInputFlagLogin: React.FC<Props> = ({title, navigation, numeric}) => {
  const [number, onChangeNumber] = useState(null);
  const [getFlagItemData, setFlagItemData] = useState(null);
  const [getFlagIndexData, setFlagIndexData] = useState(null);

  const backData = (item: any, index: any) => {
    console.log('dataFromNextScreen', item, index);
    setFlagItemData(item);
    setFlagIndexData(index);
  };
  return (
    <View>
      <View style={styles.labelContainer}>
        <Text>{title}</Text>
      </View>
      <View style={styles.inputContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() =>
            navigation.navigate('ScreenCountry', {
              id: 20,
              test: 'anything you want here',
              backData: backData,
            })
          }>
          <Image
            source={
              getFlagItemData && getFlagItemData.source
                ? getFlagItemData.source
                : Icons.icFlagOne
            }
          />
          <Text style={styles.count}>
            +{getFlagIndexData ? getFlagIndexData : 1}
          </Text>
          <Image source={Icons.icArrowDown} />
          <View style={styles.straightLine} />
        </TouchableOpacity>

        <TextInput
          placeholder="1586 4585 8745 324"
          onChangeText={onChangeNumber}
          style={styles.input}
          keyboardType={numeric}
        />
      </View>
    </View>
  );
};

export default TextInputFlagLogin;

const styles = StyleSheet.create({
  labelContainer: {
    backgroundColor: 'white',
    alignSelf: 'flex-start',
    paddingHorizontal: 10,
    marginLeft: 10,
    zIndex: 1,
    elevation: 1,
    shadowColor: 'white',
    position: 'absolute',
    top: -12,
    fontSize: 14,
    color: Colors.blackText,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 8,
    padding: 8,
    zIndex: 0,
    marginBottom: 16,
    height: 54,
  },
  input: {
    flex: 9,
    height: 40,
    padding: 10,
  },
  button: {
    flex: 3,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  count: {marginHorizontal: 10},
  straightLine: {
    width: 1,
    height: 22,
    backgroundColor: Colors.blackText,
    marginLeft: 10,
  },
});
