/* eslint-disable react-hooks/rules-of-hooks */
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import Colors from '../../constaints/Colors';
import Icon from 'react-native-vector-icons/Ionicons';
interface Props {
  flex?: any;
  marginRight?: number;
  marginBottom?: number;
  titleText?: String;
  borderColor?: any;
  placeholder?: any;
  checkPassword?: boolean;
  numeric?: any;
  backgroundColor?: string;
  callGetValueTextInput?: any;
}
const TextInputLogin: React.FC<Props> = ({
  flex,
  marginRight,
  marginBottom,
  titleText,
  borderColor,
  placeholder,
  checkPassword,
  numeric,
  backgroundColor,
  callGetValueTextInput,
}) => {
  // const [number, onChangeNumber] = useState('');
  const [hidePass, setHidePass] = useState(true);

  return (
    <View
      style={{
        flex: flex,
        marginRight: marginRight,
        marginBottom: marginBottom,
      }}>
      <View style={styles.labelContainer}>
        <Text>{titleText}</Text>
      </View>
      <View
        style={[
          styles.inputContainer,
          {
            borderColor: borderColor ? borderColor : null,
            backgroundColor: backgroundColor ? backgroundColor : Colors.while,
          },
        ]}>
        <TextInput
          style={styles.input}
          placeholder={placeholder}
          onChangeText={newText => {
            callGetValueTextInput(newText);
          }}
          // onSubmitEditing={addText => {
          //   // console.log('number   ', number);

          //   callGetValueTextInput(addText);
          // }}
          secureTextEntry={hidePass && checkPassword ? true : false}
          keyboardType={numeric}
        />
        {checkPassword ? (
          <TouchableOpacity
            style={{marginRight: 24}}
            onPress={() => {
              setHidePass(!hidePass);
            }}>
            <Icon
              name={hidePass ? 'eye-off-outline' : 'eye-outline'}
              size={20}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  labelContainer: {
    backgroundColor: Colors.while,
    alignSelf: 'flex-start',
    paddingHorizontal: 10,
    marginLeft: 10,
    zIndex: 1,
    elevation: 1,
    shadowColor: Colors.while,
    position: 'absolute',
    top: -12,
    fontSize: 14,
    color: Colors.blackText,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 8,
    padding: 8,
    zIndex: 0,
    // backgroundColor: Colors.while,
    // marginBottom: 16,
    height: 54,
  },
  input: {
    flex: 9,
    height: 40,
    padding: 10,
  },
});
export default TextInputLogin;
