import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';

interface Props {
  source?: any;
  onPress?: any;
  valueTitle?: any;
  value?: any;
}

const BtnSelectRole: React.FC<Props> = props => {
  return (
    <TouchableOpacity style={styles.btnSelectRole} onPress={props.onPress}>
      <Image source={props.source} style={styles.icBusiness} />
      <View>
        <Text style={styles.textBottomOne} maxLength={1}>
          {props.valueTitle}
        </Text>
        <Text style={styles.textBottomTwo} maxLength={1}>
          {props.value}
        </Text>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  btnSelectRole: {
    flexDirection: 'row',
    width: 327,
    height: 76,
    marginBottom: 16,
    marginHorizontal: 24,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#9090AD',
  },
  icBusiness: {
    width: 24,
    height: 26,
    marginVertical: 23,
    marginLeft: 12,
    marginRight: 10,
    resizeMode: 'cover',
  },
  textBottomOne: {
    marginTop: 16,
    marginBottom: 4,
    fontSize: 16,
    fontWeight: '500',
  },
  textBottomTwo: {
    fontSize: 13,
    fontWeight: '400',
  },
});
export default BtnSelectRole;
