import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import Buttons from './Buttons';
import Colors from '../../constaints/Colors';

interface Props {
  handleGetValueEmployee?: any;
  dataEmployee?: any;
}

const BtnSelectbottomSheet: React.FC<Props> = props => {
  return (
    <View style={styles.container}>
      <FlatList
        style={styles.containerFlatList}
        data={props.dataEmployee}
        renderItem={value => (
          <TouchableOpacity
            style={styles.containerBtnText}
            onPress={() => props.handleGetValueEmployee(value.item.value)}>
            <Text>{value.item.value}</Text>
          </TouchableOpacity>
        )}
        keyExtractor={item => item.value}
      />
      <View style={styles.containerBtn}>
        <Buttons
          backgroundColor={Colors.blueStraight}
          height={44}
          content={'Add Employee'}
          fontSizeContent={20}
          fontWeightContent={'600'}
          color={Colors.while}
          checkICon={true}
          borderTopLeftRadius={10}
          borderTopRightRadius={10}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1},
  containerFlatList: {flex: 9},
  containerBtnText: {
    marginHorizontal: 20,
    marginTop: 20,
  },
  containerBtn: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
    flex: 1,
  },
});
export default BtnSelectbottomSheet;
