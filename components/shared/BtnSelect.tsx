import {StyleSheet, Text, View, TouchableOpacity, Modal} from 'react-native';
import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import ModalPicker from './ModalPicker';
import Colors from '../../constaints/Colors';

interface Props {
  placeholder?: any;
  title?: any;
  icon?: any;
  data?: any;
  borderColor?: any;
}
const BtnSelect: React.FC<Props> = ({
  placeholder,
  title,
  icon,
  data,
  borderColor,
}) => {
  const [chooseData, setChooseData] = useState(placeholder);
  const [isModalVisible, setisModalVisible] = useState(false);
  const changeModalVisibility = (bool: boolean) => {
    setisModalVisible(bool);
  };
  const setData = (option: any) => {
    setChooseData(option);
  };
  return (
    <View>
      <View style={styles.labelContainer}>
        <Text>{title}</Text>
      </View>
      <TouchableOpacity
        style={[styles.pickerContainer, {borderColor: borderColor}]}
        onPress={() => changeModalVisibility(true)}>
        <Text style={styles.contentPickker}>{chooseData}</Text>
        <Icon name={icon} size={24} />
      </TouchableOpacity>
      <Modal
        animationType="slide"
        transparent={true}
        visible={isModalVisible}
        onRequestClose={() => changeModalVisibility(false)}>
        <ModalPicker
          changeModalVisibility={changeModalVisibility}
          setData={setData}
          data={data}
        />
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  labelContainer: {
    backgroundColor: 'white',
    alignSelf: 'flex-start',
    paddingHorizontal: 10,
    marginLeft: 10,
    zIndex: 1,
    elevation: 1,
    shadowColor: 'white',
    position: 'absolute',
    top: -12,
    fontSize: 14,
    color: Colors.blackText,
  },
  pickerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 20,
    zIndex: 0,
    marginBottom: 16,
    height: 54,
  },
  contentPickker: {
    fontSize: 14,
    color: '#9090AD',
  },
});
export default BtnSelect;
