import {StyleSheet, Text, TouchableOpacity, View, Image} from 'react-native';
import React from 'react';
import Colors from '../../constaints/Colors';

interface Props {
  navigation?: any;
  content?: String;
  noteAppbar?: String;
  imgLeft?: any;
  imgRight?: any;
}

const AppbarDetailEmployee: React.FC<Props> = ({
  navigation,
  content,
  noteAppbar,
  imgLeft,
  imgRight,
}) => {
  return (
    <View style={[styles.Appbar]}>
      <TouchableOpacity style={styles.containerBtn}>
        <Image source={imgLeft} />
      </TouchableOpacity>
      <View style={styles.titleAppbar}>
        <Text style={styles.contentitleAppbar}>{content}</Text>
        {noteAppbar ? (
          <Text style={styles.noteAppbar}>{noteAppbar}</Text>
        ) : null}
      </View>
      <TouchableOpacity
        style={styles.containerBtn}
        onPress={() => navigation.goBack()}>
        <Image source={imgRight} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  Appbar: {
    marginTop: 17,
    marginHorizontal: 24,
    flexDirection: 'row',
  },
  titleAppbar: {flex: 8},
  contentitleAppbar: {
    marginTop: 5,
    fontSize: 20,
    fontWeight: '700',
    color: Colors.while,
  },
  noteAppbar: {
    fontSize: 14,
    fontWeight: '500',
    color: Colors.while,
  },
  containerBtn: {
    marginTop: 12,
    marginRight: 12,
    width: 40,
    height: 40,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default AppbarDetailEmployee;
