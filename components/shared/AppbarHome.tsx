import {StyleSheet, Text, TouchableOpacity, View, Image} from 'react-native';
import React from 'react';
import Colors from '../../constaints/Colors';

interface Props {
  navigation?: any;
  content?: String;
  noteAppbar?: String;
  imgLeft?: any;
  imgRight?: any;
}

const AppbarHome: React.FC<Props> = ({
  content,
  noteAppbar,
  imgLeft,
  imgRight,
  navigation,
}) => {
  return (
    <View style={[styles.Appbar]}>
      <TouchableOpacity
        style={styles.containerBtn}
        onPress={() => {
          navigation.openDrawer();
        }}>
        <Image source={imgLeft} />
      </TouchableOpacity>
      <View style={styles.titleAppbar}>
        <Text style={styles.contentitleAppbar}>{content}</Text>
        {noteAppbar ? (
          <Text style={styles.noteAppbar}>{noteAppbar}</Text>
        ) : null}
      </View>
      <TouchableOpacity style={styles.containerBtn}>
        <Image source={imgRight} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  Appbar: {
    marginTop: 17,
    marginHorizontal: 24,
    flexDirection: 'row',
  },
  titleAppbar: {flex: 8},
  contentitleAppbar: {
    marginTop: 5,
    fontSize: 20,
    fontWeight: '500',
    color: Colors.while,
  },
  noteAppbar: {
    fontSize: 20,
    fontWeight: '700',
    color: Colors.while,
  },
  containerBtn: {
    marginTop: 12,
    marginRight: 12,
    width: 40,
    height: 40,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
  },
});
export default AppbarHome;
