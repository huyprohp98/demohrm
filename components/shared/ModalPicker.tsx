import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import React, {useState} from 'react';
import Colors from '../../constaints/Colors';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

interface modalPickerProps {
  changeModalVisibility?: any;
  setData?: any;
  data?: any;
}

const ModalPicker: React.FC<modalPickerProps> = props => {
  const onPressItem = (option: any) => {
    const data = getSelect.map((item: any) => {
      console.log(`item:    ${JSON.stringify(item)}`);
      return {
        ...item,
        isSelected: option.value === item.value,
      };
    });
    console.log(`data:    ${JSON.stringify(data)}`);
    setSelect(data);

    setTimeout(() => {
      props.changeModalVisibility(false);
      props.setData(option.value);
    }, 200);
  };
  const [getSelect, setSelect] = useState(props.data);

  const option = getSelect.map((item: any, index: any) => {
    return (
      <View style={styles.option}>
        <Text style={styles.text}>{item.value}</Text>
        <TouchableOpacity
          style={[
            styles.containerBorderSelect,
            {
              borderColor: item.isSelected ? '#567DF4' : '#9090AD',
            },
          ]}
          key={index}
          onPress={() => onPressItem(item)}>
          <View
            style={[
              styles.containerSelect,
              {
                backgroundColor: item.isSelected ? '#567DF4' : null,
              },
            ]}
          />
        </TouchableOpacity>
      </View>
    );
  });
  return (
    <TouchableOpacity
      onPress={() => props.changeModalVisibility(false)}
      style={styles.container}>
      <View style={[styles.modal, {width: WIDTH / 2, height: HEIGHT / 4}]}>
        <ScrollView>{option}</ScrollView>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginRight: 24,
  },
  modal: {
    backgroundColor: Colors.while,
    borderRadius: 10,
  },
  option: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: {
    margin: 20,
    fontSize: 18,
    fontWeight: '600',
  },
  containerBorderSelect: {
    width: 16,
    height: 16,
    borderRadius: 50,
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerSelect: {
    width: 10,
    height: 10,
    borderRadius: 50,
  },
});
export default ModalPicker;
