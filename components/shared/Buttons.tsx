import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

interface Props {
  navigation?: any;
  backgroundColor?: any;
  marginTop?: Number;
  marginBottom?: Number;
  marginRight?: Number;
  height?: Number;
  width?: any;
  color?: any;
  borderRadius?: Number;
  borderTopLeftRadius?: number;
  borderTopRightRadius?: Number;
  fontSize?: any;
  nameOnpress?: any;
  content?: String;
  fontWeightContent?: String;
  fontSizeContent?: Number;
  borderWidth?: Number;
  borderColor?: any;
  checkICon?: boolean;
  flex?: any;
  onPress?: any;
}
const Buttons: React.FC<Props> = props => {
  const propsStyle = {
    backgroundColor: props.backgroundColor,
    marginTop: props.marginTop,
    marginBottom: props.marginBottom,
    marginRight: props.marginRight,
    height: props.height,
    width: props.width,
    borderRadius: props.borderRadius,
    borderWidth: props.borderWidth,
    borderColor: props.borderColor,
    borderTopLeftRadius: props.borderTopLeftRadius,
    borderTopRightRadius: props.borderTopRightRadius,
  };
  const propsContent = {
    color: props.color,
    fontWeight: props.fontWeightContent,
    fontSize: props.fontSize,
  };
  return (
    <TouchableOpacity
      style={[styles.button, propsStyle, {flex: props.flex}]}
      onPress={props.onPress}>
      {props.checkICon ? (
        <Icon name={'add-outline'} size={20} style={{color: props.color}} />
      ) : null}
      <Text style={[styles.content, propsContent]}> {props.content}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  content: {},
});
export default Buttons;
