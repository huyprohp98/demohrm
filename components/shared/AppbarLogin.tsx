import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import Colors from '../../constaints/Colors';
import Icons from '../../constaints/Icons';
interface Props {
  navigation: any;
  content?: string;
  noteAppbar?: string;
  iconRight?: boolean;
}
const AppbarLogin: React.FC<Props> = ({
  navigation,
  content,
  noteAppbar,
  iconRight,
}) => {
  return (
    <View style={[styles.Appbar]}>
      <View style={styles.titleAppbar}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Image source={Icons.icArrowLeft} style={styles.icArrowLeft} />
        </TouchableOpacity>
        <Text style={styles.contentitleAppbar}>{content}</Text>
        {iconRight ? (
          <TouchableOpacity onPress={() => {}} style={styles.containerSeach}>
            <Image source={Icons.icSearch} />
          </TouchableOpacity>
        ) : null}
      </View>
      {noteAppbar ? <Text style={styles.noteAppbar}>{noteAppbar}</Text> : null}
    </View>
  );
};
const styles = StyleSheet.create({
  Appbar: {
    marginTop: 17,
    marginLeft: 24,
  },
  titleAppbar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icArrowLeft: {
    width: 24,
    height: 24,
    marginRight: 12,
    resizeMode: 'cover',
    tintColor: Colors.while,
  },
  contentitleAppbar: {
    fontSize: 20,
    fontWeight: '700',
    color: Colors.while,
    flex: 8,
  },
  noteAppbar: {
    marginTop: 12,
    fontSize: 14,
    fontWeight: '400',
    color: Colors.while,
  },

  containerSeach: {
    flex: 1,
    width: 40,
    height: 40,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 20,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
  },
});
export default AppbarLogin;
