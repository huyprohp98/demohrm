import {StyleSheet, View} from 'react-native';
import React from 'react';
import AppbarLogin from '../shared/AppbarLogin';
import TextInputFlagLogin from '../shared/TextInputFlagLogin';
import Buttons from '../shared/Buttons';
import Colors from '../../constaints/Colors';

interface Props {
  navigation?: any;
}

const ScreenForgotPassword: React.FC<Props> = ({navigation}) => {
  return (
    <View style={styles.container}>
      <AppbarLogin
        navigation={navigation}
        content={'Forgot Password'}
        noteAppbar={'Lorem ipsum dolor sit amet, consectetur.'}
      />
      <View style={styles.containerContent}>
        <TextInputFlagLogin title={'Mobile Number'} navigation={navigation} />
        <Buttons
          backgroundColor={Colors.blueStraight}
          marginTop={30}
          marginBottom={16}
          height={54}
          borderRadius={10}
          content={'Get OTP'}
          fontSizeContent={20}
          fontWeightContent={'600'}
          color={Colors.while}
          onPress={() => {
            navigation.navigate('ScreenOtp', {name: 'ScreenOtp'});
          }}
        />
      </View>
    </View>
  );
};

export default ScreenForgotPassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.while,
    paddingHorizontal: 24,
    paddingTop: 40,
  },
});
