import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Images from '../constaints/Images';
import BtnSelectRole from './shared/BtnSelectRole';
import Icons from '../constaints/Icons';

interface Props {
  navigation?: any;
}

const SelectRoleSISU: React.FC<Props> = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Image source={Images.imgLogoSelectRole} style={styles.imgLogo} />
      <Image source={Images.imgPepoleSelectRole} style={styles.imgPeople} />
      <Text style={styles.title}>Select Your Role</Text>
      <BtnSelectRole
        onPress={() =>
          navigation.navigate('SignUpManage', {name: 'SignUpManage'})
        }
        valueTitle="Business Owner / Admin / HR"
        value={'Register your company & start attendance'}
        source={Icons.icBusinessSelectRole}
      />
      <BtnSelectRole
        onPress={() =>
          navigation.navigate('SignUpEmployee', {name: 'SignUpEmployee'})
        }
        valueTitle="Employee"
        value={'Register and start marking your attendance'}
        source={Icons.icManSelectRole}
      />
    </View>
  );
};

export default SelectRoleSISU;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f9f1f2',
  },
  imgLogo: {
    marginTop: 48,
    marginBottom: 72,
    marginHorizontal: 130,
    width: 119,
    height: 69,
    resizeMode: 'cover',
  },
  imgPeople: {
    width: 279,
    height: 190,
    marginHorizontal: 49,
    resizeMode: 'cover',
  },
  title: {
    marginTop: 60,
    marginHorizontal: 102,
    marginBottom: 20,
    fontSize: 22,
    fontWeight: '600',
  },
});
