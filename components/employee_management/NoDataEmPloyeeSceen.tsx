import {StyleSheet, View, Image, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import AppbarLogin from '../shared/AppbarLogin';
import Colors from '../../constaints/Colors';
import Images from '../../constaints/Images';
import Icons from '../../constaints/Icons';

interface Props {
  route?: any;
  navigation?: any;
}

const NoDataEmPloyeeSceen: React.FC<Props> = props => {
  return (
    <View style={styles.container}>
      <AppbarLogin
        navigation={props.navigation}
        content={props.route.params.title}
      />
      <View style={styles.containerContent}>
        <View style={styles.containerContentMenu}>
          <View style={styles.image}>
            <Image source={Images.imgNoDataEmployee} />
            <Text style={styles.titleContent}>No Data</Text>
            <Text style={styles.content}>Add your employee</Text>
          </View>
          <TouchableOpacity
            style={styles.btnAdd}
            onPress={() => {
              if (props.route.params.title === 'Add Employee') {
                props.navigation.navigate('AddEmloyeeScreen', {
                  name: 'AddEmloyeeScreen',
                });
              } else if (props.route.params.title === 'Expenses') {
                props.navigation.navigate('AddExpensesScreen', {
                  name: 'AddExpensesScreen',
                });
              }
            }}>
            <Image source={Icons.icAddEmployee} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.whileBackground,
    paddingTop: 24,
  },
  containerContentMenu: {
    flex: 1,
    marginTop: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 24,
    paddingTop: 24,
  },
  image: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleContent: {
    fontSize: 24,
    fontWeight: '600',
    color: '#22215B',
    marginTop: 30,
  },
  content: {
    fontSize: 14,
    fontWeight: '500',
    marginTop: 2,
    color: '#9090AD',
  },
  btnAdd: {position: 'absolute', bottom: 60, right: 34},
});
export default NoDataEmPloyeeSceen;
