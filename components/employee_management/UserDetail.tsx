import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Colors from '../../constaints/Colors';
import Icons from '../../constaints/Icons';
import AppbarDetailEmployee from '../shared/AppbarDetailEmployee';
import Buttons from '../shared/Buttons';
import TextInputLogin from '../shared/TextInputLogin';

interface Props {
  route?: any;
  navigation?: any;
}

const UserDetail: React.FC<Props> = props => {
  const data = props.route.params.data;
  return (
    <View style={styles.container}>
      <AppbarDetailEmployee
        content={data.titleName}
        noteAppbar={data.titleContent}
        imgLeft={data.image}
        imgRight={Icons.icDelete}
        navigation={props.navigation}
      />
      <View style={styles.containerContent}>
        <View style={styles.containerInputTop}>
          <TextInputLogin
            titleText={'Per Day'}
            placeholder={'$570.00'}
            borderColor={'rgba(34, 33, 91, 0.16)'}
            marginBottom={16}
            flex={5}
            marginRight={15}
          />
          <TextInputLogin
            titleText={'Employee ID'}
            placeholder={'1234'}
            borderColor={'rgba(34, 33, 91, 0.16)'}
            marginBottom={16}
            flex={5}
            marginRight={15}
          />
        </View>
        <View style={styles.containerContentInformation}>
          <View style={styles.information}>
            <Text style={styles.textInformation}>Joining Date</Text>
            <Text style={styles.textInformation}>Reference</Text>
            <Text style={styles.textInformation}>Contact No</Text>
          </View>
          <View style={styles.straight} />
          <View style={styles.information}>
            <Text>10-06-2021</Text>
            <Text>Ibne Riead</Text>
            <Text>+1254 2415 156</Text>
          </View>
        </View>
        <View style={styles.containerContentInformation}>
          <View style={[styles.information, {justifyContent: 'center'}]}>
            <Text style={styles.textInformation}>Working day</Text>
          </View>
          <View style={styles.straight} />
          <View style={[styles.information, {justifyContent: 'center'}]}>
            <Text>Mon, Tue, Wed, Thu, San</Text>
          </View>
        </View>
        <View style={styles.btnBottom}>
          <Buttons
            marginRight={10}
            backgroundColor={Colors.blueStraight}
            height={54}
            borderRadius={10}
            content={'Delete'}
            fontSizeContent={20}
            fontWeightContent={'600'}
            color={Colors.while}
            flex={5}
          />
          <Buttons
            backgroundColor={'rgba(86, 125, 244, 0.1)'}
            height={54}
            borderRadius={10}
            content={'Edit'}
            fontSizeContent={20}
            fontWeightContent={'600'}
            color={Colors.while}
            flex={5}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: '#FAFAFA',
    paddingTop: 40,
  },
  containerContentMenu: {
    flex: 9,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    marginTop: 20,
    paddingHorizontal: 24,
    paddingTop: 24,
    backgroundColor: '#FFFFFF',
    // position: 'absolute',
  },
  containerInputTop: {flexDirection: 'row', marginHorizontal: 24},
  containerContentInformation: {
    marginHorizontal: 24,
    marginBottom: 24,
    height: 136,
    backgroundColor: Colors.while,
    borderRadius: 20,
    paddingHorizontal: 16,
    paddingVertical: 24,
  },
  information: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  straight: {
    height: 1,
    backgroundColor: '#22215B',
    marginTop: 10,
    marginBottom: 20,
  },
  textInformation: {
    fontSize: 14,
    fontWeight: '600',
    color: '#22215B',
  },
  btnBottom: {
    flexDirection: 'row',
    marginHorizontal: 24,
    position: 'absolute',
    bottom: 10,
  },
});
export default UserDetail;
