import React from 'react';
import {Linking, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Colors from '../../constaints/Colors';
import AppbarLogin from '../shared/AppbarLogin';
import Buttons from '../shared/Buttons';

interface Props {
  navigation?: any;
}
const EmployeeAddSuccessScreen: React.FC<Props> = ({navigation}) => {
  const handleOpenWithLinking = ({
    link,
    phone,
  }: {
    link?: string;
    phone?: string;
  }) => {
    link != null ? Linking.openURL(`${link}`) : Linking.openURL(`tel:${phone}`);
  };

  return (
    <View style={styles.container}>
      <AppbarLogin
        navigation={navigation}
        content={'Employee Added Successfully'}
      />
      <View style={styles.containerContent}>
        <View style={styles.contentPayStore}>
          <Text style={styles.textPayStoreTop}>
            Please download app form Playstore
          </Text>
          <Text style={styles.textContent}>
            Please clicking on the below link:
          </Text>
          <TouchableOpacity
            onPress={() =>
              handleOpenWithLinking({
                link: 'https://reactnative.dev/docs/linking',
              })
            }>
            <Text style={[styles.textContent, {color: '#567DF4'}]}>
              httpl://bit.ly/hryu3a
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.contentPayStore}>
          <Text style={styles.textPayStoreTop}>
            Please download app form Playstore
          </Text>
          <View style={styles.containerTextMid}>
            <Text style={[styles.textContent, {color: '#22215B'}]}>User: </Text>
            <TouchableOpacity
              onPress={() =>
                handleOpenWithLinking({
                  phone: '0869258726',
                })
              }>
              <Text style={styles.textContent}>+1452 4521 5412</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.containerTextMid}>
            <Text style={[styles.textContent, {color: '#22215B'}]}>
              Password:
            </Text>
            <Text style={styles.textContent}> D5z145destg</Text>
          </View>
        </View>
        <Buttons
          backgroundColor={Colors.blueStraight}
          marginTop={30}
          marginBottom={16}
          height={54}
          borderRadius={10}
          content={'Share Details With Employee'}
          fontSizeContent={20}
          fontWeightContent={'600'}
          color={Colors.while}
          nameOnpress={'EmployeeListScreen'}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: '#FAFAFA',
    paddingHorizontal: 24,
    paddingTop: 40,
  },
  contentPayStore: {
    height: 112,
    backgroundColor: Colors.while,
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingTop: 20,
    paddingBottom: 32,
    marginBottom: 20,
  },
  textPayStoreTop: {
    fontSize: 16,
    fontWeight: '500',
    color: '#22215B',
    marginBottom: 4,
  },
  textContent: {
    fontSize: 13,
    fontWeight: '400',
    color: '#9090AD',
    marginBottom: 4,
  },
  containerTextMid: {flexDirection: 'row'},
});
export default EmployeeAddSuccessScreen;
