import React from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import Colors from '../../constaints/Colors';
import AppbarLogin from '../shared/AppbarLogin';
import TextInputLogin from '../shared/TextInputLogin';
import Buttons from '../shared/Buttons';
import Icon from 'react-native-vector-icons/Ionicons';
import Images from '../../constaints/Images';

const DATA_MENU = [
  {
    id: '1',
    image: Images.imgPersonOneEmployee,
    titleName: 'Client Management',
    titleContent: 'Designer',
  },
  {
    id: '2',
    image: Images.imgPersonTwoEmployee,
    titleName: 'NOC/Ex Certificate',
    titleContent: 'Manager',
  },
  {
    id: '3',
    image: Images.imgPersonThreeEmployee,
    titleName: 'Notice Board',
    titleContent: 'Developer',
  },
  {
    id: '4',
    image: Images.imgPersonFourEmployee,
    titleName: 'Award',
    titleContent: 'Officer',
  },
];

interface Props {
  navigation?: any;
}
interface itemProps {
  item?: any;
}
const EmployeeListScreen: React.FC<Props> = props => {
  const renderItemMenu: React.FC<itemProps> = ({item}) => (
    <View style={styles.containerList}>
      <TouchableOpacity
        style={styles.containerBtn}
        onPress={() => {
          props.navigation.navigate('UserDetail', {data: item});
        }}>
        <View
          style={[
            styles.containerIconBtn,
            {backgroundColor: item.colorContainerIcon},
          ]}>
          <Image source={item.image} />
        </View>
        <View style={styles.containerContentText}>
          <Text style={styles.contentText}>{item.titleName}</Text>
          <Text
            style={[
              styles.contentText,
              {color: '#9090AD', fontSize: 13, fontWeight: '400'},
            ]}>
            {item.titleContent}
          </Text>
        </View>

        <Icon
          name={'chevron-forward-outline'}
          size={24}
          style={{marginRight: 25}}
        />
      </TouchableOpacity>
    </View>
  );
  return (
    <View style={styles.container}>
      <AppbarLogin
        navigation={props.navigation}
        content={'Employee List'}
        iconRight={true}
      />
      <View style={styles.containerContent}>
        <View style={{flex: 1, flexDirection: 'row', marginHorizontal: 24}}>
          <TextInputLogin
            titleText={'Full Name'}
            placeholder={'Maan Theme'}
            borderColor={'rgba(34, 33, 91, 0.16)'}
            marginBottom={16}
            flex={5}
            marginRight={15}
          />
          <Buttons
            backgroundColor={Colors.blueStraight}
            height={54}
            borderRadius={10}
            content={'Add Employee'}
            fontSizeContent={20}
            fontWeightContent={'600'}
            color={Colors.while}
            checkICon={true}
            flex={5}
          />
        </View>
        <View style={styles.containerContentMenu}>
          <FlatList
            data={DATA_MENU}
            renderItem={renderItemMenu}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: '#FAFAFA',
    paddingTop: 40,
  },
  containerContentMenu: {
    flex: 9,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    marginTop: 20,
    paddingHorizontal: 24,
    paddingTop: 24,
    backgroundColor: '#FFFFFF',
    // position: 'absolute',
  },
  containerList: {
    marginBottom: 16,
  },
  containerBtn: {
    height: 56,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'rgba(34, 33, 91, 0.16)',
    backgroundColor: Colors.while,
    flexDirection: 'row',
    alignItems: 'center',
  },
  colorBorderRight: {
    position: 'absolute',
    width: 4,
    height: 56,
    borderBottomLeftRadius: 6,
    borderTopLeftRadius: 6,
  },
  containerIconBtn: {
    marginHorizontal: 16,
    marginTop: 16,
    marginBottom: 12,
    borderRadius: 6,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerContentText: {flex: 8},
  contentText: {
    marginHorizontal: 16,
    fontSize: 16,
    fontWeight: '400',
    color: '#22215B',
  },
});
export default EmployeeListScreen;
