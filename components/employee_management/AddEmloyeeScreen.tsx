/* eslint-disable react/self-closing-comp */
import {StyleSheet, View, Image, ScrollView} from 'react-native';
import React from 'react';
import Colors from '../../constaints/Colors';
import AppbarLogin from '../shared/AppbarLogin';
import TextInputLogin from '../shared/TextInputLogin';
import Images from '../../constaints/Images';
import Icons from '../../constaints/Icons';
import BtnSelectDate from '../shared/BtnSelectDate';
import Buttons from '../shared/Buttons';
import BtnSelect from '../shared/BtnSelect';
interface Props {
  navigation: any;
}
const AddEmloyeeScreen: React.FC<Props> = ({navigation}) => {
  const dataDesignation = [
    {value: 'Designer', isSelected: true},
    {value: 'Manager', isSelected: false},
    {value: 'Developer', isSelected: false},
    {value: 'Officer', isSelected: false},
  ];
  const dataWorkingDay = [
    {value: 'Friday', isSelected: true},
    {value: 'Saturday', isSelected: false},
    {value: 'Sunday', isSelected: false},
    {value: 'Monday', isSelected: false},
    {value: 'Tuesday', isSelected: false},
    {value: 'Wednesday', isSelected: false},
    {value: 'Thursday', isSelected: false},
  ];
  const dataGender = [
    {value: 'Male', isSelected: true},
    {value: 'Female', isSelected: false},
  ];
  return (
    <ScrollView style={styles.container}>
      <AppbarLogin navigation={navigation} content={'Add Employee'} />
      <View style={styles.containerContent}>
        <View style={styles.containerContentMenu}>
          <View style={[styles.containerTopSelect, {marginBottom: 44}]}>
            <View style={styles.containerImage}>
              <Image source={Images.imgAvatarEmployee} style={styles.avatar} />
              <Image style={styles.changeAvatar} source={Icons.icChangeImage} />
            </View>
            <View style={{flex: 6}}>
              <BtnSelectDate
                title={'Joining Date'}
                placeholder={'01/10/2020'}
                borderColor={'rgba(34, 33, 91, 0.16)'}></BtnSelectDate>
              <TextInputLogin
                titleText={'Employee ID'}
                placeholder={'1254'}
                borderColor={'rgba(34, 33, 91, 0.16)'}
              />
            </View>
          </View>
          <TextInputLogin
            titleText={'Full Name'}
            placeholder={'Maan Theme'}
            borderColor={'rgba(34, 33, 91, 0.16)'}
            marginBottom={16}
          />
          <TextInputLogin
            titleText={'Mobile Number'}
            placeholder={'+1245 2458 1554 224'}
            borderColor={'rgba(34, 33, 91, 0.16)'}
            numeric={'numeric'}
            marginBottom={16}
          />
          <BtnSelect
            title={'Designation'}
            placeholder={'01/10/2020'}
            borderColor={'rgba(34, 33, 91, 0.16)'}
            data={dataDesignation}
            icon={'chevron-down-outline'}
          />
          <BtnSelect
            title={'Working Day'}
            placeholder={'01/10/2020'}
            borderColor={'rgba(34, 33, 91, 0.16)'}
            data={dataWorkingDay}
            icon={'chevron-down-outline'}
          />
          <View style={styles.containerPay}>
            <TextInputLogin
              titleText={'Basic Pay'}
              placeholder={'$00.00'}
              borderColor={'rgba(34, 33, 91, 0.16)'}
              numeric={'numeric'}
              flex={3}
              marginRight={10}
            />
            <View style={styles.btnMidPay}>
              <Buttons
                backgroundColor={Colors.blueStraight}
                height={54}
                borderRadius={10}
                content={'Per Day'}
                fontSizeContent={20}
                fontWeightContent={'600'}
                color={Colors.while}
              />
            </View>
            <View style={styles.btnRightPay}>
              <Buttons
                navigation={navigation}
                height={54}
                borderRadius={10}
                content={'Monthly'}
                fontSizeContent={20}
                fontWeightContent={'600'}
                borderWidth={1}
                borderColor={'#567DF4'}
                color={'#567DF4'}
              />
            </View>
          </View>
          <BtnSelect
            title={'Gender'}
            placeholder={'Male'}
            borderColor={'rgba(34, 33, 91, 0.16)'}
            data={dataGender}
            icon={'chevron-down-outline'}
          />
          <BtnSelect
            title={'Add Reference'}
            placeholder={'Enter Reference Name'}
            borderColor={'rgba(34, 33, 91, 0.16)'}
            data={dataDesignation}
            icon={'add-outline'}
          />
          <Buttons
            backgroundColor={Colors.blueStraight}
            height={54}
            borderRadius={10}
            content={'Save'}
            fontSizeContent={20}
            fontWeightContent={'600'}
            color={Colors.while}
            navigation={navigation}
            onPress={() => {
              navigation.navigate('EmployeeAddSuccessScreen', {
                name: 'EmployeeAddSuccessScreen',
              });
            }}
            // nameOnpress={'Employee Added Successfully'}
          />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.whileBackground,
    paddingTop: 24,
  },
  containerContentMenu: {
    flex: 1,
    marginTop: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 24,
    paddingTop: 24,
  },
  containerTopSelect: {
    flexDirection: 'row',
  },
  containerImage: {flex: 4, width: 100, height: 100},
  avatar: {resizeMode: 'cover'},
  changeAvatar: {
    width: 34,
    height: 34,
    borderRadius: 100,
    marginLeft: 70,
    marginTop: 70,
    position: 'absolute',
    borderWidth: 2,
    borderColor: '#FFFFFF',
  },
  containerPay: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 16,
  },
  btnMidPay: {flex: 2, marginRight: 10},
  btnRightPay: {flex: 2},
});
export default AddEmloyeeScreen;
