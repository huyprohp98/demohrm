/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/self-closing-comp */
import {DrawerContentScrollView} from '@react-navigation/drawer';
import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../../constaints/Colors';
import Icons from '../../constaints/Icons';
import Images from '../../constaints/Images';

const {height} = Dimensions.get('window');

const dataMenu = [
  {
    id: '1',
    iconLeft: Icons.icSettings,
    content: 'Setting',
    iconRight: Icons.icBackRight,
  },
  {
    id: '2',
    iconLeft: Icons.icGroup,
    content: 'Premium Version',
    noteContent: '(PRO)',
    iconRight: Icons.icBackRight,
  },
  {
    id: '3',
    iconLeft: Icons.icCoffee,
    content: 'Holiday',
    iconRight: Icons.icBackRight,
  },
  {
    id: '4',
    iconLeft: Icons.icLock,
    content: 'App Lock',
  },
  {
    id: '5',
    iconLeft: Icons.icUsersFriend,
    content: 'Share With Friends',
    iconRight: Icons.icBackRight,
  },
  {
    id: '6',
    iconLeft: Icons.icAlertCircle,
    content: 'Terms of Services',
    iconRight: Icons.icBackRight,
  },
  {
    id: '7',
    iconLeft: Icons.icAlertTriangle,
    content: 'Privacy Policy',
    iconRight: Icons.icBackRight,
  },
  {
    id: '8',
    iconLeft: Icons.icLogOut,
    content: 'Log Out',
    iconRight: Icons.icBackRight,
  },
];
interface drawerProps {
  navigation?: any;
}

const CustomDrawerContent: React.FC<drawerProps> = props => {
  const [saveAcount, setSaveAcount] = useState(false);

  return (
    <DrawerContentScrollView {...props}>
      <View style={styles.container}>
        <View style={styles.containerÌnormation}>
          <View style={styles.containerProfile}>
            <Image
              source={Images.imgPersonOneEmployee}
              style={styles.imgProfile}></Image>
            <Text style={styles.text}>Shaidul Islam</Text>
            <Text>Admin</Text>
          </View>
          <View style={styles.caculateInfomation}>
            <View style={styles.containerInformation}>
              <Text
                style={[styles.text, {fontWeight: '700', color: Colors.while}]}>
                27
              </Text>
              <Text
                style={[
                  styles.text,
                  {fontWeight: '400', color: Colors.while, fontSize: 14},
                ]}>
                Employee
              </Text>
            </View>
            <View style={styles.containerInformation}>
              <Text
                style={[styles.text, {fontWeight: '700', color: Colors.while}]}>
                12
              </Text>
              <Text
                style={[
                  styles.text,
                  {fontWeight: '400', color: Colors.while, fontSize: 14},
                ]}>
                Client
              </Text>
            </View>
            <View style={styles.containerInformation}>
              <Text
                style={[styles.text, {fontWeight: '700', color: Colors.while}]}>
                50
              </Text>
              <Text
                style={[
                  styles.text,
                  {fontWeight: '400', color: Colors.while, fontSize: 14},
                ]}>
                Total File
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.containerBtnBack}>
          {dataMenu.map((item, index) => {
            return index === 3 ? (
              <View
                style={{
                  flexDirection: 'row',
                  marginHorizontal: 24,
                  marginVertical: 18,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    flex: 1,
                    marginRight: 24,
                  }}>
                  <Image source={item.iconLeft}></Image>
                </View>
                <View
                  style={{
                    flex: 9,
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                  }}>
                  <Text>{item.content}</Text>
                  <Text>{item.noteContent}</Text>
                </View>
                <View style={styles.containerSave}>
                  <TouchableOpacity
                    onPress={() => {
                      setSaveAcount(!saveAcount);
                    }}
                    style={[
                      styles.containerSaveAcount,
                      {
                        backgroundColor:
                          saveAcount === false
                            ? '#9090AD'
                            : Colors.blueStraight,
                      },
                    ]}>
                    <View
                      style={[
                        styles.saveAcount,
                        {
                          backgroundColor:
                            saveAcount === true
                              ? Colors.blueStraight
                              : Colors.while,
                        },
                      ]}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  marginHorizontal: 24,
                  marginVertical: 18,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <View style={{flex: 1, marginRight: 24}}>
                  <Image source={item.iconLeft}></Image>
                </View>
                <View
                  style={{
                    flex: 9,
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                  }}>
                  <Text>{item.content}</Text>
                  {index === 1 ? <Text>{item.noteContent}</Text> : null}
                </View>
                <View style={{flex: 1}}>
                  <Image
                    source={item.iconRight}
                    style={{marginLeft: 15}}></Image>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    </DrawerContentScrollView>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, height: height},
  containerÌnormation: {
    flex: 4,
    backgroundColor: Colors.blueStraight,
    borderBottomRightRadius: 24,
    borderBottomLeftRadius: 24,
    marginTop: -5,
  },
  containerProfile: {
    width: 295,
    height: 193,
    backgroundColor: Colors.whileBackground,
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderBottomRightRadius: 24,
    borderBottomLeftRadius: 24,
  },
  imgProfile: {
    width: 80,
    height: 80,
    marginTop: 20,
    resizeMode: 'cover',
  },
  text: {
    fontSize: 20,
    fontWeight: '600',
    color: '#22215B',
  },
  caculateInfomation: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 16,
    marginHorizontal: 44,
  },
  containerInformation: {alignItems: 'center'},
  containerBtnBack: {
    flex: 7,
    backgroundColor: Colors.while,
    marginTop: 23,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  containerSave: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerSaveAcount: {
    width: 32,
    height: 16,
    borderRadius: 8,
    borderWidth: 1,
    justifyContent: 'center',
  },
  saveAcount: {
    width: 8,
    height: 8,
    borderRadius: 100,
    marginLeft: 4,
  },
});
export default CustomDrawerContent;
