import React from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../../constaints/Colors';
import Icons from '../../constaints/Icons';
import AppbarHome from '../shared/AppbarHome';
import ListManage from './ListManage';

const DATA_MENU = [
  {
    id: '1',
    colorBorderRight: '#4DCEFA',
    colorContainerIcon: 'rgba(83, 208, 251, 0.1)',
    icon: Icons.icEmployeeHome,
    titleName: 'Client Management',
  },
  {
    id: '2',
    colorBorderRight: '#FF8919',
    colorContainerIcon: 'rgba(255, 137, 26, 0.08)',
    icon: Icons.icExpensesHome,
    titleName: 'NOC/Ex Certificate',
  },
  {
    id: '3',
    colorBorderRight: '#07BB85',
    colorContainerIcon: 'rgba(76, 227, 100, 0.08)',
    icon: Icons.icPayrollHome,
    titleName: 'Notice Board',
  },
  {
    id: '4',
    colorBorderRight: '#8270F1',
    colorContainerIcon: 'rgba(130, 112, 241, 0.05)',
    icon: Icons.icFolderHome,
    titleName: 'Award',
  },
];

interface homeProps {
  navigation?: any;
}
interface itemProps {
  item?: any;
}

const HomeScreen: React.FC<homeProps> = ({navigation}) => {
  const renderItemMenu: React.FC<itemProps> = ({item}) => (
    <View style={styles.containerList}>
      <TouchableOpacity style={styles.containerBtn}>
        <View
          style={[
            styles.colorBorderRight,
            {backgroundColor: item.colorBorderRight},
          ]}
        />
        <View
          style={[
            styles.containerIconBtn,
            {backgroundColor: item.colorContainerIcon},
          ]}>
          <Image source={item.icon} />
        </View>
        <Text style={styles.contentBtn}>{item.titleName}</Text>

        <Icon
          name={'chevron-forward-outline'}
          size={24}
          style={{marginRight: 60}}
        />
      </TouchableOpacity>
    </View>
  );
  return (
    <View style={styles.container}>
      <AppbarHome
        content={'HRM &'}
        noteAppbar={'Payroll Management'}
        imgLeft={Icons.icListHome}
        imgRight={Icons.icNotificationHome}
        navigation={navigation}
      />
      <View style={styles.containerContent}>
        <View>
          <ListManage navigation={navigation} />
        </View>
        <View style={styles.containerContentMenu}>
          <FlatList
            data={DATA_MENU}
            renderItem={renderItemMenu}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.whileBackground,
    paddingTop: 40,
  },
  containerContentMenu: {
    flex: 1,
    marginTop: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 24,
    paddingTop: 24,
  },
  containerList: {
    marginBottom: 16,
  },

  containerBtn: {
    width: Dimensions.get('window').width,
    height: 56,
    borderRadius: 6,
    backgroundColor: Colors.while,
    flexDirection: 'row',
    alignItems: 'center',
  },
  colorBorderRight: {
    position: 'absolute',
    width: 4,
    height: 56,
    borderBottomLeftRadius: 6,
    borderTopLeftRadius: 6,
  },
  containerIconBtn: {
    marginHorizontal: 16,
    marginTop: 16,
    marginBottom: 12,
    borderRadius: 6,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentBtn: {
    flex: 8,
    marginHorizontal: 16,
    fontSize: 16,
    fontWeight: '400',
    color: '#22215B',
  },
});
export default HomeScreen;
