import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import React from 'react';
import Icons from '../../constaints/Icons';
import Colors from '../../constaints/Colors';

const DATA_MANAGEMENT = [
  {
    id: '0',
    colorBorderRight: '#7C69EE',
    colorContainerIcon: '#9D8CFE',
    icon: Icons.icEmployeeHome,
    titleName: 'Employee',
    titleBottom: 'Management',
  },
  {
    id: '1',
    colorBorderRight: '#FD72AF',
    colorContainerIcon: '#FF9BC8',
    icon: Icons.icExpensesHome,
    titleName: 'Expenses',
    titleBottom: 'Management',
  },
  {
    id: '2',
    colorBorderRight: '#4ACDF9',
    colorContainerIcon: '#81DFFF',
    icon: Icons.icPayrollHome,
    titleName: 'Payroll',
    titleBottom: 'Management',
  },
  {
    id: '3',
    colorBorderRight: '#7C69EE',
    colorContainerIcon: '#9D8CFE',
    icon: Icons.icFolderHome,
    titleName: 'File',
    titleBottom: 'Management',
  },
];

interface listManage {
  navigation?: any;
}
interface itemManage {
  item?: any;
  index?: any;
}
const ListManage: React.FC<listManage> = ({navigation}) => {
  const renderItemManage: React.FC<itemManage> = ({item, index}) => (
    <View style={styles.containerList}>
      <TouchableOpacity
        style={styles.containerBtn}
        onPress={() => {
          console.log(index);
          if (index === 0) {
            navigation.navigate('SplashEmployeeScreen', {
              name: 'SplashEmployeeScreen',
            });
          } else if (index === 1) {
            navigation.navigate('ScreenExpenses', {
              name: 'ScreenExpenses',
            });
          }
        }}>
        <View
          style={[
            styles.colorBorderRight,
            {backgroundColor: item.colorBorderRight},
          ]}
        />
        <View
          style={[
            styles.containerIconBtn,
            {backgroundColor: item.colorContainerIcon},
          ]}>
          <Image source={item.icon} />
        </View>
        <Text style={styles.contentBtn}>{item.titleName}</Text>
        <Text style={styles.contentBtn}>{item.titleBottom}</Text>
      </TouchableOpacity>
    </View>
  );
  return (
    <FlatList
      numColumns={2}
      data={DATA_MANAGEMENT}
      renderItem={renderItemManage}
      keyExtractor={item => item.id}
    />
  );
};

const styles = StyleSheet.create({
  containerList: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 18,
    paddingHorizontal: 24,
  },
  containerBtn: {
    width: 155,
    height: 128,
    borderRadius: 10,
    backgroundColor: Colors.while,
  },
  colorBorderRight: {
    position: 'absolute',
    width: 4,
    height: 128,
    borderBottomLeftRadius: 6,
    borderTopLeftRadius: 6,
  },
  containerIconBtn: {
    marginHorizontal: 16,
    marginTop: 16,
    marginBottom: 12,
    borderRadius: 6,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentBtn: {
    marginHorizontal: 16,
    fontSize: 16,
    fontWeight: '400',
  },
});
export default ListManage;
