import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import Colors from '../constaints/Colors';
import Images from '../constaints/Images';
import Icons from '../constaints/Icons';

const arrNexts = [0, 1, 2];

interface Props {
  navigation?: any;
}

const OnbroadingScreen: React.FC<Props> = ({navigation}) => {
  const [count, setCount] = useState(0);

  return (
    <View style={styles.container}>
      <View style={styles.containerTop}>
        <View style={{flexDirection: 'row'}}>
          <Text style={{flex: 8}} />
          {count === 0 || count === 1 ? (
            <TouchableOpacity
              style={styles.buttonSkip}
              onPress={() => navigation.navigate('SelectRoleSISU')}>
              <Text>Skip</Text>
            </TouchableOpacity>
          ) : (
            <Text style={styles.buttonSkip} />
          )}
        </View>
        <Image
          source={
            count === 0
              ? Images.imageOnbroadMan
              : count === 1
              ? Images.imageOnbroadGirl
              : Images.imageOnbroadPeople
          }
          style={styles.imageMan}
        />
      </View>
      <View style={styles.next}>
        <Text style={styles.textTitle}>
          {count === 0
            ? 'Keep healthy\nwork-life balance'
            : count === 1
            ? 'Track your work &\nget result'
            : 'Stay organized\nwith team'}
        </Text>
        <Text maxLength={3} style={styles.textContent}>
          {
            'Lorem ipsum dolor sit amet, consectetur adi\npiscing elit. Cursus sit suspendisse aliquam eget\nlorem viverra tincidunt.'
          }
        </Text>

        <View style={styles.nextBottom}>
          <View style={{flexDirection: 'row'}}>
            {arrNexts.map((straight, index) => (
              <View
                key={index}
                style={[styles.straight, {width: count === index ? 30 : 11}]}
              />
            ))}
          </View>
          <TouchableOpacity
            onPress={() => {
              if (count < 2) {
                setCount(count + 1);
                console.log('<2', count);
              } else {
                setCount(0);
                navigation.navigate('SelectRoleSISU', {name: 'SelectRoleSISU'});
                console.log('=2', count);
              }
            }}>
            <View style={styles.containerBottom}>
              <Image
                source={
                  count === 0
                    ? Icons.icCycleBroadOne
                    : count === 1
                    ? Icons.icCycleBroadTwo
                    : Icons.icCycleBroadThree
                }
              />
            </View>
          </TouchableOpacity>
        </View>
        <View />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 100,
    backgroundColor: Colors.backgroudOnbroad,
    opacity: 0.9,
  },
  containerTop: {
    flex: 60,
  },
  next: {
    flex: 50,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.while,
    paddingHorizontal: 24,
  },
  buttonSkip: {
    flex: 1,
    marginTop: 36,
    marginRight: 27,
    fontSize: 16,
    color: Colors.blackText,
  },
  imageMan: {
    width: 343,
    height: 266,
    marginHorizontal: 16,
    resizeMode: 'contain',
  },
  textTitle: {
    marginTop: 30,
    marginBottom: 16,
    fontSize: 30,
    fontWeight: '800',
    color: Colors.charcoalPurpleTitle,
  },
  textContent: {
    marginBottom: 40,
    color: Colors.whileTextContent,
    fontSize: 14,
  },
  nextBottom: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  straight: {
    marginTop: 82,
    marginRight: 5,
    borderColor: Colors.blueStraight,
    borderBottomWidth: 1,
    borderWidth: 5,
    borderRadius: 5,
  },
  containerBottom: {
    marginTop: 38,
  },
  iconBottom: {
    position: 'absolute',
  },
});
export default OnbroadingScreen;
