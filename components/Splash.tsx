import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

const Splash = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Splash</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroudColor: 'green',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
  },
});
export default Splash;
