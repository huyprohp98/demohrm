import {StyleSheet, Text, TextInput, View} from 'react-native';
import React, {useState, useRef, useEffect} from 'react';
import AppbarLogin from './shared/AppbarLogin';
import Colors from '../constaints/Colors';
import Buttons from './shared/Buttons';

interface Props {
  navigation?: any;
}

const ScreenOtp: React.FC<Props> = ({navigation}) => {
  const [textOne, onChangeTextOne] = useState('');
  const [textTwo, onChangeTextTwo] = useState('');
  const [textThree, onChangeTextThree] = useState('');
  const [textFour, onChangeTextFour] = useState('');
  const [textFive, onChangeTextFive] = useState('');
  const [textSix, onChangeTextSix] = useState('');
  const refInput2 = useRef();
  const refInput3 = useRef();
  const refInput4 = useRef();
  const refInput5 = useRef();
  const refInput6 = useRef();
  const [getCheckOtp, setCheckOtp] = useState([]);
  const [countDown, setCountDown] = useState(120);

  useEffect(() => {
    const timerId = setInterval(() => {
      setCountDown(countDown - 1);
    }, 1000);
    if (countDown === 0) {
      clearInterval(timerId);
    }
    return () => clearInterval(timerId);
  }, [countDown]);

  return (
    <View style={styles.container}>
      <AppbarLogin
        navigation={navigation}
        content={'Mobile Number Verify'}
        noteAppbar={'Enter the 6 digit code sent to your\nmobile number'}
      />
      <View style={styles.containerContent}>
        <View style={styles.title}>
          <View style={styles.colorRightTitle} />
          <Text style={styles.textTitle}>Otp input</Text>
        </View>
        <View style={styles.containerTextInput}>
          <TextInput
            style={styles.textInput}
            keyboardType="numeric"
            maxLength={1}
            autoFocus={true}
            onChangeText={value => {
              getCheckOtp.push(value);
              setCheckOtp(getCheckOtp);
              onChangeTextOne(value);
              refInput2.current.focus();
            }}
            value={textOne}
            blurOnSubmit={false}
          />
          <TextInput
            style={styles.textInput}
            keyboardType="numeric"
            maxLength={1}
            onChangeText={value => {
              getCheckOtp.push(value);
              setCheckOtp(getCheckOtp);
              onChangeTextTwo(value);
              refInput3.current.focus();
            }}
            value={textTwo}
            onSubmitEditing={() => refInput3.current.focus()}
            ref={refInput2}
          />
          <TextInput
            style={styles.textInput}
            keyboardType="numeric"
            maxLength={1}
            onChangeText={value => {
              getCheckOtp.push(value);
              setCheckOtp(getCheckOtp);
              onChangeTextThree(value);
              refInput4.current.focus();
            }}
            value={textThree}
            onSubmitEditing={() => refInput4.current.focus()}
            ref={refInput3}
          />
          <TextInput
            style={styles.textInput}
            keyboardType="numeric"
            maxLength={1}
            onChangeText={value => {
              getCheckOtp.push(value);
              setCheckOtp(getCheckOtp);
              onChangeTextFour(value);
              refInput5.current.focus();
            }}
            value={textFour}
            onSubmitEditing={() => refInput5.current.focus()}
            ref={refInput4}
          />
          <TextInput
            style={styles.textInput}
            keyboardType="numeric"
            maxLength={1}
            onChangeText={value => {
              getCheckOtp.push(value);
              setCheckOtp(getCheckOtp);
              onChangeTextFive(value);
              refInput6.current.focus();
            }}
            value={textFive}
            onSubmitEditing={() => refInput6.current.focus()}
            ref={refInput5}
          />
          <TextInput
            style={styles.textInput}
            keyboardType="numeric"
            maxLength={1}
            onChangeText={value => {
              getCheckOtp.push(value);
              setCheckOtp(getCheckOtp);
              onChangeTextSix(value);
            }}
            value={textSix}
            ref={refInput6}
          />
        </View>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Buttons
            backgroundColor={'rgba(34, 33, 91, 0.1)'}
            marginTop={24}
            marginBottom={16}
            height={42}
            width={152}
            borderRadius={6}
            content={'Resend OTP'}
            fontSizeContent={16}
            fontWeightContent={'500'}
            color={Colors.blackText}
            onPress={() => {
              setCountDown(120);
            }}
          />
        </View>
        <Text style={styles.textTime}>
          You can request OTP after {countDown}
        </Text>
        <Buttons
          backgroundColor={Colors.blueStraight}
          marginTop={30}
          marginBottom={16}
          height={54}
          borderRadius={10}
          content={'Verify'}
          fontSizeContent={20}
          fontWeightContent={'600'}
          color={Colors.while}
          onPress={() => {
            if (getCheckOtp.length === 6) {
              navigation.navigate('SignIn', {name: 'SignIn'});
              onChangeTextOne();
              onChangeTextTwo();
              onChangeTextFour();
              onChangeTextThree();
              onChangeTextFive();
              onChangeTextSix();
              setCountDown(120);
            } else {
              null;
            }
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.blueStraight,
  },

  containerContent: {
    flex: 1,
    marginTop: 36,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: Colors.while,
    paddingHorizontal: 24,
    paddingTop: 40,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  title: {
    height: 54,
    borderRadius: 6,
    backgroundColor: 'rgba(255, 137, 26, 0.1)',
  },
  colorRightTitle: {
    position: 'absolute',
    backgroundColor: '#FF891A',
    width: 6,
    height: 54,
    borderBottomLeftRadius: 6,
    borderTopLeftRadius: 6,
  },
  textTitle: {
    marginLeft: 18,
    marginVertical: 13,
    fontSize: 20,
    fontWeight: '600',
  },
  containerTextInput: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 24,
    justifyContent: 'space-between',
  },
  textInput: {
    width: 49,
    height: 56,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#22215B',
    paddingHorizontal: 18,
    paddingVertical: 12,
  },
  textTime: {
    marginHorizontal: 80,
    fontWeight: '600',
    fontSize: 14,
    color: '#FF8919',
    marginBottom: 30,
  },
});
export default ScreenOtp;
