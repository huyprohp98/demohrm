export default {
  icCycleBroadOne: require('../assets/icons/ic_cycle_broad_one.png'),
  icCycleBroadTwo: require('../assets/icons/ic_cycle_broad_two.png'),
  icCycleBroadThree: require('../assets/icons/ic_cycle_broad_three.png'),
  icBusinessSelectRole: require('../assets/icons/ic_business_select_role.png'),
  icManSelectRole: require('../assets/icons/ic_man_select_role.png'),
  icArrowLeft: require('../assets/icons/ic_arrow_left.png'),
  icFlagOne: require('../assets/icons/ic_flag_one.png'),
  icArrowDown: require('../assets/icons/ic_arrow_down.png'),
  icFlagAustralia: require('../assets/icons/ic_flag_australia.png'),
  icFlagCanada: require('../assets/icons/ic_flag_canada.png'),
  icFlagFrance: require('../assets/icons/ic_flag_france.png'),
  icFlagGermany: require('../assets/icons/ic_flag_germany.png'),
  icFlagItaly: require('../assets/icons/ic_flag_italy.png'),
  icFaTurkey: require('../assets/icons/ic_flag_turkey.png'),
  icFlagUnitedKingdom: require('../assets/icons/ic_flag_united_kingdom.png'),
  icFlagUnitedStates: require('../assets/icons/ic_flag_united_states.png'),
  icFlagUruguay: require('../assets/icons/ic_flag_uruguay.png'),
  icFlagBangladesh: require('../assets/icons/ic_flag_bangladesh.png'),
  icFlagIndia: require('../assets/icons/ic_flag_india.png'),
  icListHome: require('../assets/icons/ic_list_home.png'),
  icEmployeeHome: require('../assets/icons/ic_employee_home.png'),
  icExpensesHome: require('../assets/icons/ic_expenses_home.png'),
  icPayrollHome: require('../assets/icons/ic_payroll_home.png'),
  icFolderHome: require('../assets/icons/ic_folder_home.png'),
  icGoogleSignup: require('../assets/icons/ic_google_signup.png'),
  icTwitterSignup: require('../assets/icons/ic_twitter_signup.png'),
  icFacebookSignup: require('../assets/icons/ic_facebook_signup.png'),
  icNotificationHome: require('../assets/icons/ic_notification_home.png'),
  icExitES: require('../assets/icons/ic_exit_es.png'),
  icReferralES: require('../assets/icons/ic_referral_es.png'),
  icSalaryES: require('../assets/icons/ic_salary_es.png'),
  icImmigrationES: require('../assets/icons/ic_immigration_es.png'),
  icWorkingTimeES: require('../assets/icons/ic_working_time_es.png'),
  icYouthES: require('../assets/icons/ic_youth_es.png'),
  icDate: require('../assets/icons/ic_date.png'),
  icChangeImage: require('../assets/icons/ic_change_image.png'),
  icAddEmployee: require('../assets/icons/ic_add_employee.png'),
  icSearch: require('../assets/icons/ic_search.png'),
  icDelete: require('../assets/icons/ic_delete.png'),
  icSettings: require('../assets/icons/ic_settings.png'),
  icBackRight: require('../assets/icons/ic_back_right.png'),
  icGroup: require('../assets/icons/ic_group.png'),
  icCoffee: require('../assets/icons/ic_coffee.png'),
  icLock: require('../assets/icons/ic_lock.png'),
  icUsersFriend: require('../assets/icons/ic_users_friend.png'),
  icAlertCircle: require('../assets/icons/ic_alert_circle.png'),
  icAlertTriangle: require('../assets/icons/ic_alert_triangle.png'),
  icLogOut: require('../assets/icons/ic_log_out.png'),
};
