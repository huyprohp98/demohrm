export default {
  imageOnbroadMan: require('../assets/images/img_onbroad_man.png'),
  imageOnbroadGirl: require('../assets/images/img_onbroad_girl.png'),
  imageOnbroadPeople: require('../assets/images/img_onbroad_people.png'),
  imgLogoSelectRole: require('../assets/images/img_logo_select_role.png'),
  imgPepoleSelectRole: require('../assets/images/img_pepole_select_role.png'),
  imgAvatarEmployee: require('../assets/images/img_avatar_employee.png'),
  imgNoDataEmployee: require('../assets/images/img_no_data_employee.png'),
  imgPersonOneEmployee: require('../assets/images/img_person_one_employee.png'),
  imgPersonTwoEmployee: require('../assets/images/img_person_two_employee.png'),
  imgPersonThreeEmployee: require('../assets/images/img_person_three_employee.png'),
  imgPersonFourEmployee: require('../assets/images/img_person_four_employee.png'),
  imgLogoTitleHrm: require('../assets/images/img_logo_title_hrm.png'),
};
