export default {
  backgroudOnbroad: '#D7E4F9',
  primary: '#ED6263',
  blackText: '#363C49',
  while: '#FFFFFF',
  whileBackground: '#FAFAFA',
  charcoalPurpleTitle: '#22215B',
  whileTextContent: '#9090AD',
  blueStraight: '#567DF4',
};
