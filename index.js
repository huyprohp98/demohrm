/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
// import Splash from './components/Splash';
// import 'react-native-gesture-handler';

AppRegistry.registerComponent(appName, () => App);
