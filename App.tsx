import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import ProfileScreen from './components/ProfileScreen ';
import SplashScreen from 'react-native-splash-screen';
import {useEffect} from 'react';
import OnbroadingScreen from './components/OnbroadingScreen';
import SignIn from './components/login/SignIn';
import SelectRoleSISU from './components/SelectRoleSISU';
import SignUpEmployee from './components/register/SignUpEmployee';
import ScreenCountry from './components/register/ScreenCountry';
import ScreenOtp from './components/ScreenOtp';
import ScreenForgotPassword from './components/forgot_password/ScreenForgotPassword';
import SignUpManage from './components/register/SignUpManage';
import HomeScreen from './components/home/HomeScreen ';
import SplashEmployeeScreen from './components/employee_management/SplashEmployeeScreen';
// import test from './components/test';
import NoDataEmPloyeeSceen from './components/employee_management/NoDataEmPloyeeSceen';
import EmployeeListScreen from './components/employee_management/EmployeeListScreen';
import UserDetail from './components/employee_management/UserDetail';
import ScreenExpenses from './components/expenses_management/ScreenExpenses';
import AddExpensesScreen from './components/expenses_management/AddExpensesScreen';
import AddEmloyeeScreen from './components/employee_management/AddEmloyeeScreen';
import EmployeeAddSuccessScreen from './components/employee_management/EmployeeAddSuccessScreen';
import ExpensesListScreen from './components/expenses_management/ExpensesListScreen';
import ExpensesDetail from './components/expenses_management/ExpensesDetail';
import ExpensesPDF from './components/expenses_management/ExpensesPDF';
import CustomDrawerContent from './components/home/CustomDrawerContent';
// import AuthStack from './components/navigation/AuthStack';

const Drawer = createDrawerNavigator();

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <NavigationContainer>
      <Drawer.Navigator
        screenOptions={{
          headerShown: false,
          drawerStyle: {
            width: 295,
            backgroundColor: '#FAFAFA',
          },
        }}
        initialRouteName={'Home'}
        drawerContent={props => <CustomDrawerContent {...props} />}>
        {/* <Drawer.Screen name="test" component={test} /> */}
        <Drawer.Screen name="Home" component={HomeScreen} />

        <Drawer.Screen name="Profile" component={ProfileScreen} />
        <Drawer.Screen name="SelectRoleSISU" component={SelectRoleSISU} />
        <Drawer.Screen name="SignUpEmployee" component={SignUpEmployee} />
        <Drawer.Screen name="SignUpManage" component={SignUpManage} />
        <Drawer.Screen name="SignIn" component={SignIn} />
        <Drawer.Screen name="ScreenCountry" component={ScreenCountry} />
        <Drawer.Screen name="ScreenOtp" component={ScreenOtp} />
        <Drawer.Screen
          name="ScreenForgotPassword"
          component={ScreenForgotPassword}
        />
        <Drawer.Screen
          name="SplashEmployeeScreen"
          component={SplashEmployeeScreen}
        />
        <Drawer.Screen
          name="NoDataEmPloyeeSceen"
          component={NoDataEmPloyeeSceen}
        />
        <Drawer.Screen name="AddEmloyeeScreen" component={AddEmloyeeScreen} />
        <Drawer.Screen
          name="EmployeeAddSuccessScreen"
          component={EmployeeAddSuccessScreen}
        />
        <Drawer.Screen
          name="EmployeeListScreen"
          component={EmployeeListScreen}
        />
        <Drawer.Screen name="UserDetail" component={UserDetail} />
        <Drawer.Screen name="ScreenExpenses" component={ScreenExpenses} />
        <Drawer.Screen name="AddExpensesScreen" component={AddExpensesScreen} />
        <Drawer.Screen
          name="ExpensesListScreen"
          component={ExpensesListScreen}
        />
        <Drawer.Screen name="ExpensesDetail" component={ExpensesDetail} />
        <Drawer.Screen name="ExpensesPDF" component={ExpensesPDF} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default App;
